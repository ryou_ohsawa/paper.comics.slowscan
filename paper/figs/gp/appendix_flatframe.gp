#!/usr/local/bin/gnuplot
set terminal pdfcairo enh col font 'Ubuntu,12' size 12cm,8cm
set output '../append_flatframe.pdf'
set encoding iso

unset log x
unset log y

set xr [394:407]
set yr [400:450]
set xtics auto
set ytics auto

set xl 'Averaged Pixel Counts (10^{3}ADU)' font ',14' offset 0,0.2
set yl 'Pixel Counts (10^{3}ADU)' font ',14' offset .6,0
set lmargin 8.
set rmargin 2.
set tmargin .5
set bmargin 3.
set grid x y
set key font ',12' samplen 3 left Left rev
unset bar

F='../data/flatvaliditycheck.tbl'
plot \
  F u ($1/1e3):($2/1e3):(sqrt($2*350)/350e3) w yerror \
  lc rgb 'coral' pt 6 ps 0.3 t "(i, j) = (244, 47)", \
  F u ($1/1e3):($3/1e3) w line lw 2 lc rgb 'gray10' not, \
  F u ($1/1e3):($4/1e3):(sqrt($4*350)/350e3) w yerror \
  lc rgb 'royalblue' pt 4 ps 0.3 t "(i, j) = (313, 117)", \
  F u ($1/1e3):($5/1e3) w line lw 2 lc rgb 'gray10' not, \
  F u ($1/1e3):($6/1e3):(sqrt($6*350)/350e3) w yerror \
  lc rgb 'forest-green' pt 2 ps 0.4 t "(i, j) = (169, 47)", \
  F u ($1/1e3):($7/1e3) w line lw 2 lc rgb 'gray10' not
# EOF
