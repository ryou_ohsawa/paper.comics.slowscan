#!/usr/bin/gnuplot
set encoding iso
set terminal pdfcairo font 'Ubuntu,12' size 8,2

C='../data/1719+186_chop_phot.tbl'
S='../data/1719+186_scan2_birsvd_phot.tbl'

h(x,n,d) = (x>n*d)?h(x,n+1,d):(n*d-d/2.)

set lmargin 8
set rmargin 2
set tmargin 1
set bmargin 3.5
set key font ',14'
set tics front

set output '../photometry_statistics_intensity.pdf'

set xl 'Measured Intensity (ADU)' font 'Ubuntu-Bold,16' offset 0,0.
set yl 'Frequency' font 'Ubuntu-Bold,16' offset 0.5,0

set label 1 '(A)' at graph 0, graph 1 offset 1,-1.4 font ',20'

set xtics 0,5000,1e6
set mxtics 10
set xr [4e4:8e4]
set ytics 0,0.1,1.0 format '%4.1f'
set mytics 2
set yr [0:0.4]
set boxwidth 1000

plot \
     C u (h($1,10,1000)):(1./143) s freq w boxes \
     lc rgb 'orange-red' fs solid 0.3 t 'Chopping Observation', \
     S u (h($1,10,1000)):(1./286) s freq w boxes \
     lc rgb 'royalblue' fs pattern 2 t '"Slow-Scanning" Observation', \
     C u (h($1,10,1000)):(1./143) s freq w histeps lc rgb 'orange-red' not
###

unset output


set output '../photometry_statistics_stddev.pdf'

set xl 'Standard Deviation in Blank Sky Region (ADU/pixel)' font 'Ubuntu-Bold,16' offset 0,0.
set yl 'Frequency' font 'Ubuntu-Bold,16' offset 0.5,0

set label 1 '(B)' at graph 0, graph 1 offset 1,-1.4 font ',20'

set xtics 0,50,1e3
set mxtics 10
set xr [80:210]
set ytics 0,0.1,0.5 format '%4.1f'
set mytics 2
set yr [0:0.4]
set boxwidth 2.5

plot \
     C u (h(sqrt(2)*$2,0,2.5)):(1./143) s freq w boxes \
     lc rgb 'orange-red' fs solid 0.3 t 'Chopping Observation', \
     S u (h(sqrt(1)*$2,0,2.5)):(1./286) ev ::1::286 s freq w boxes\
     lc rgb 'royalblue' fs pattern 2 t '"Slow-Scanning" Observation', \
     C u (h(sqrt(2)*$2,0,2.5)):(1./143) s freq w histeps lc rgb 'orange-red' not
###

unset output


set output '../photometry_statistics_snr.pdf'

set xl 'Signal-to-Noise Ratio' font 'Ubuntu-Bold,16' offset 0,0.
set yl 'Frequency' font 'Ubuntu-Bold,16' offset 0.5,0

set label 1 '(C)' at graph 0, graph 1 offset 1,-1.4 font ',20'

set xtics 0,5,100
set mxtics 5
set xr [10:50]
set ytics 0,0.1,0.5 format '%4.1f'
set mytics 2
set yr [0:0.8]
set boxwidth 1

plot \
     C u (h($1/sqrt(2*$3)/$2,10,1)):(1./143) s freq w boxes \
     lc rgb 'orange-red' fs solid 0.3 t 'Chopping Observation', \
     S u (h($1/sqrt($3)/$2,10,1)):(1./286) s freq w boxes\
     lc rgb 'royalblue' fs pattern 2 t '"Slow-Scanning" Observation', \
     C u (h($1/sqrt(2*$3)/$2,10,1)):(1./143) s freq w histeps \
     lc rgb 'orange-red' not
###

unset output
