#!/usr/local/bin/gnuplot
sinc(x) = (sin(pi*x)/(pi*x))
orig(x) = 1./x
sumexp(x,N) = abs(sum [n=0:(N-1)] exp(-sqrt(-1)*2*n*pi*x)/N)
chop(x,dt) = 2*(1-cos(2*pi*x*dt))
integ(x,T,N) = abs(sinc(x*T)*sumexp(2*T*x,N))**2
ESD(x,dt,r,N) = orig(x)*chop(x,dt)*integ(x,dt*r,N)

set terminal pdfcairo enh col font 'Ubuntu,12' size 12cm,6cm
set output '../append_chopping.pdf'
set encoding iso

set log x
unset log y

set xr [0.01:20]
set yr [0.0:3.2]
set sample 1e3
set xtics auto
set ytics 0,0.5,2.5 format '%.1f'

set xl 'Frequency: {f} (Hz)' font ',14' offset 0,0.2
set yl 'Normalized Power Spectrum {\327} {f}' font ',14' offset .6,0
set lmargin 7.
set rmargin 2.
set tmargin .5
set bmargin 3.
unset grid
set key font ',12' samplen 3

plot \
  1.0 lw 1 dt 4  lc rgb 'gray30' \
  t 'original noise spectrum', \
  integ(x,1.0,1) lw 2 dt 2  lc rgb 'royalblue' \
  t '1.0s integration', \
  x*ESD(x,1.0,1.0,1) lw 2 lc rgb 'royalblue' \
  t '1.0s chopping, 1.0s integration, no coadd', \
  x*ESD(x,1.0,1.0,5) lw 1.5 lc rgb 'orange-red' \
  t 'coadd 5 exposures'
#    EOF
#  integ(x,0.5,1) dt 2 lc rgb 'orange-red' not, \
