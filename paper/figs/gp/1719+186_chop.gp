#!/usr/bin/gnuplot
set encoding iso
set terminal pdfcairo font 'Ubuntu,12' size 4.6,2.9

FILE='../data/1719+186_chop_mean.raw'
set output '../1719+186_chop.pdf'

set xrange [0:299]
set yrange [0:199]
set xtics -1,50,400 format ''
set ytics -1,50,200 format ''
set tics front

set cbr [-50:500]
set cbtics 0,100,1000
set cbtics font ',10' offset -.8,0
set colorbox user
set colorbox origin first 302, first 0
set colorbox size char 2, graph 1
set size ratio -1
set lmargin 1
set rmargin 7
set tmargin 0
set bmargin 0

set label 1 front '(B)' center at graph 0, graph 1 offset 4,-2 \
font 'Ubuntu,32' tc rgb 'white'

set style arrow 1 front lw 2 lc rgb 'white' fill
set arrow 2 from 280,180 to 260,180 as 1
set arrow 3 from 280,180 to 280,160 as 1
set label 2 front 'N' center at 260,180 \
font 'Ubuntu-Bold,18' offset -1.5,0 tc rgb 'white'
set label 3 front 'E' center at 280,160 \
font 'Ubuntu-Bold,18' offset 0,-1.0 tc rgb 'white'

set style arrow 2 front lw 2 lc rgb 'white' nohead
set arrow 10 from 240,180 to 240-38.46,180 as 2
set label 10 front '5"' center at 240-38.46/2.0,180 \
font 'Ubuntu-Bold,16' offset 0,.7 tc rgb 'white'

x0=95.646
y0=76.080
set object 1 circle front at x0,y0 size 11 \
  fs empty border lc rgb 'royalblue'
set object 2 circle front at x0,y0 size 13 \
  fs empty border lw .5 dt 2 lc rgb 'white'
set object 3 circle front at x0,y0 size 21 \
  fs empty border lw .5 dt 2 lc rgb 'white'

set palette gray
plot FILE binary array=300x200 format='%float' with image not

set cbr [-10:40]
set cbtics -100,10,100
unset object
set output '../1719+186_chop_hc.pdf'
plot FILE binary array=300x200 format='%float' with image not

unset output
