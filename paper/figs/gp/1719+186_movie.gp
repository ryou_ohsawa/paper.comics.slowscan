#!/usr/bin/gnuplot
set encoding iso
set terminal pdfcairo font 'Ubuntu,12' size 8,5.2

FMT='../data/scan_birsvd.%02d.raw'
set output '../1719+186_movie.pdf'

set xrange [0:299]
set yrange [50:199]
set xtics -1,50,400 format ''
set ytics -1,50,200 format ''
set tics front

set cbr [-50:2000]
set cbtics 0,200,2000
set cbtics font ',10' offset -.8,0
unset colorbox
set lmargin 0
set rmargin 0
set tmargin 0
set bmargin 0


set size 1
set origin 0,0

set palette gray

set multiplot

set size 0.3,0.3
set size ratio -1

set label 1 front '1' right at graph 1, graph 1 offset -1,-1\
font 'Ubuntu-Bold,16' tc rgb 'white'

set origin 0.00,-0.01
set label 1 front '10'
plot sprintf(FMT,10) binary array=300x200 format='%float' with image not
set origin 0.31,-0.01
set label 1 front '11'
plot sprintf(FMT,11) binary array=300x200 format='%float' with image not
set origin 0.62,-0.01
set label 1 front '12'
plot sprintf(FMT,12) binary array=300x200 format='%float' with image not

set origin 0.00,0.235
set label 1 front '7'
plot sprintf(FMT,7) binary array=300x200 format='%float' with image not
set origin 0.31,0.235
set label 1 front '8'
plot sprintf(FMT,8) binary array=300x200 format='%float' with image not
set origin 0.62,0.235
set label 1 front '9'
plot sprintf(FMT,9) binary array=300x200 format='%float' with image not

set origin 0.00,0.480
set label 1 front '4'
plot sprintf(FMT,4) binary array=300x200 format='%float' with image not
set origin 0.31,0.480
set label 1 front '5'
plot sprintf(FMT,5) binary array=300x200 format='%float' with image not
set label 1 front '6'
set origin 0.62,0.480
plot sprintf(FMT,6) binary array=300x200 format='%float' with image not

set origin 0.00,0.725
set label 1 front '1'
plot sprintf(FMT,1) binary array=300x200 format='%float' with image not
set origin 0.31,0.725
set label 1 front '2'
plot sprintf(FMT,2) binary array=300x200 format='%float' with image not
set origin 0.62,0.725
set label 1 front '3'

set colorbox user
set colorbox origin first 310, screen 0.05
set colorbox size char 4, screen 0.90

plot sprintf(FMT,3) binary array=300x200 format='%float' with image not

unset multiplot
unset output
exit

