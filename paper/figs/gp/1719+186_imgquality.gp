#!/usr/bin/gnuplot
set encoding iso

F='../data/1719+186_chop_mean.raw'
FC='../data/image_contour_chop.tbl'
G='../data/1719+186_scan2_birsvd_subch.raw'
GC='../data/image_contour_birsvd.tbl'

unset surface
set contour

x1=95.646; y1=76.080; s1=10.92
x2=177.718; y2=167.258; s2=8.03
set cntrparam levels disc 5*s1, 10*s1, 20*s1, 40*s1, 80*s1, 160*s1, 320*s1
set xr [x1-20:x1+20]; set yr [y1-20:y1+20]
set table FC
splot F binary array=300x200 format='%f' with l t 'Chop Image'
unset table
set cntrparam levels disc 5*s2, 10*s2, 20*s2, 40*s2, 80*s2, 160*s2, 320*s2
set xr [x2-20:x2+20]; set yr [y2-20:y2+20]
set table GC
splot G binary array=300x200 format='%f' with l t 'Chop Image'
unset table
unset contour

set xtics -1,50,400 format ''
set ytics -1,50,200 format ''
set tics front

set cbr [-100:2000]
set cbtics 0,100,1000
set cbtics font ',10' offset -.8,0
set colorbox user
set colorbox origin first 302, first 0
set colorbox size char 2, graph 1
set size ratio -1
set lmargin 0; set rmargin 0;
set tmargin 0; set bmargin 0

set palette gray

set terminal pdfcairo font 'Ubuntu,12' size 4.9,2.6
set output '../image_quality.pdf'

set size 1
set origin 0,0

set multiplot
set size 0.5,1

set label 1 front center at graph 0, graph 1 offset 3.5,-1.5 \
font 'Ubuntu,26' tc rgb 'white'

set origin 0,0
set label 1 '(A)'
set style arrow 1 front lw 2 lc rgb 'white' nohead
set arrow 10 from x1+25,y1-30 to x1+25-38.46,y1-30 as 1
set label 10 front '5"' center at x1+25-38.46/2.0,y1-30 \
font 'Ubuntu-Bold,16' offset 0,.7 tc rgb 'white'

set style arrow 2 front lw 1 lc rgb 'white' filled
set arrow 21 from x1+25,y1+25 to x1+25-8,y1+25 as 2
set arrow 22 from x1+25,y1+25 to x1+25,y1+25-8 as 2
set label 21 front 'N' center at x1+25-8,y1+25 \
font 'Ubuntu,14' offset -0.2,0.7 tc rgb 'white'
set label 22 front 'E' center at x1+25,y1+25-8 \
font 'Ubuntu,14' offset 1.2,-0.0 tc rgb 'white'

set xr [x1-30:x1+30]; set yr [y1-35:y1+30]
plot F binary array=300x200 format='%float' with image not, \
     FC w l lc rgb 'orange-red' not

set origin 0.5,0
set label 1 '(B)'
set style arrow 1 front lw 2 lc rgb 'white' nohead
set arrow 10 from x2+25,y2-30 to x2+25-38.46,y2-30 as 1
set label 10 front '5"' center at x2+25-38.46/2.0,y2-30 \
font 'Ubuntu-Bold,16' offset 0,.7 tc rgb 'white'

set style arrow 2 front lw 1 lc rgb 'white' filled
set arrow 21 from x2+25,y2+28-8 to x2+25-8,y2+28-8 as 2
set arrow 22 from x2+25,y2+28-8 to x2+25,y2+28 as 2
set label 21 front 'N' center at x2+25,y2+28 \
font 'Ubuntu,14' offset  1.2,0.0 tc rgb 'white'
set label 22 front 'E' center at x2+25-8,y2+28-8 \
font 'Ubuntu,14' offset -0.2,0.7 tc rgb 'white'

set xr [x2-30:x2+30]; set yr [y2-35:y2+30]
plot G binary array=300x200 format='%float' with image not, \
     GC w l lc rgb 'royalblue' not

unset multiplot
unset output
