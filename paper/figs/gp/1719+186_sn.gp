#!/usr/bin/gnuplot
set encoding iso
set terminal pdfcairo font 'Ubuntu,12' size 9cm,8cm

cmd='<awk "{sn+=\$1/(\$2*sqrt(\$3))} END {print sn/NR}" %s'
c=sprintf(cmd, '../data/1719+186_chop_phot.tbl')
s=sprintf(cmd, '../data/1719+186_scan2_birsvd_phot.tbl')
C='../data/1719+186_chop_mean3d.fits.mag.tbl'
S='../data/1719+186_scan2_birsvd_mean3d.fits.mag.tbl'

set xlabel 'Stacked Frames' font 'Ubuntu-Bold,16' offset 0.0,0.3
set ylabel 'Signal-to-Noise Ratio' font 'Ubuntu-Bold,16' offset 0.5,0.0

set xtics 0,50,400 format '%.0f'
set ytics 0,200,2000 format '%4.0f'
set mxtics 5
set mytics 5
set xr [-10:300]
set yr [0:800]
set key bottom right samplen 5 font ',14'

set sample 1000
set output '../photometric_results_evolution.pdf'

sqrtN(n) = (n>=1)?sqrt(n/22.0):1/0

R=8.;A=pi*R*R;B=4e5;F=5.5e4;G=350.
shot_noise(n) = (n>=1)?F/(sqrt(A*(B+F)*G/n)/G):1/0

plot \
     c u (1):($1/sqrt(2)) w p lw 1 \
     lc rgb 'orange-red' pt 4 ps 0.8 not, \
     s u (1):1 w p lw 1 \
     lc rgb 'royalblue' pt 6 ps 0.8 not, \
     (120*sqrtN(x)) w l lw 1 dt 2 lc rgb 'gray60' not, \
     (230*sqrtN(x)) w l lw 1 dt 4 lc rgb 'gray60' not, \
     C u (22*($0+1)):($1/$2/(sqrt($3))) w lp \
     lc rgb 'orange-red' dt 2 pt 5 ps .5 \
     t 'Chopping Observation', \
     S u (22*($0+1)):($1/$2/(sqrt($3))) w lp \
     lc rgb 'royalblue' pt 7 ps .5 \
     t '"Slow-Scanning" Observation'
###

unset output
