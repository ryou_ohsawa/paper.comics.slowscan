\subsection{Observations with \textit{Subaru}/COMICS}
\label{sec:comics}
Experimental observations were carried out on 2015-07-27 (UTC) to estimate the performance of the ``slow-scanning'' observation. We obtained the images of \objectname[IRAS 17192+1836]{IRAS~17192+1836}, a bright variable star in Hercules, with \textit{Subaru}/COMICS both in a conventional chopping observation and the ``slow-scanning'' observation. The brightness of the object at $10\,{\rm \mu m}$ is about 3\,Jy \citep{beichman_infrared_1988}, which is sufficiently bright for an imaging observation with \textit{Subaru}/COMICS. Target information is listed in Table~\ref{tab:target}. Table~\ref{tab:obslog} summarizes the observations.

\begin{table}
  \centering
  \caption{Target Information}
  \label{tab:target}
  \begin{tabular}{lr}
    \hline\hline
    Target Name
    & IRAS~17192+1836 \\
    Coordinates$^a$
    &  (17:21:26.18, +18:33:19.9) \\
    Magnitudes
    & $(B,\, V) = (12.81,\, 11.80)^a$ \\
%    & $(J,\,H,\,K_{\rm s}) = (5.1,\, 4.1,\, 3.7)^b$ \\
    IRAS Flux$^b$
    & $F_{12\,\rm{\mu m}} = 3.25\,{\rm Jy}$,
      $F_{25\,\rm{\mu m}} = 1.74\,{\rm Jy}$ \\
    \hline
    \multicolumn{2}{p{0.9\linewidth}}{$^a$the coordinates are from \citet{hog_tycho-2_2000}; $^b$the fluxes are from \citet{beichman_infrared_1988}.} \\
  \end{tabular}
\end{table}

In the chopping observation, the chopping throw angle was set to $10\arcsec$, the position angle of the chopping was set to $0\arcdeg$ (North-South), and the chopping cycle was set to $2.17\,\mathrm{s}$, \textcolor{20170927}{which has been conventionally used in the observation with \textit{Subaru}/COMICS to achieve good performance\footnote{\textcolor{20170927}{The chopping interval of $2.17\,\mathrm{s}$ is from the performance of the tip-tilt secondary mirror of the \textit{Subaru} telescope. The instability of the secondary mirror at a high frequency ($\gtrsim 1\,\mathrm{Hz}$) will cause a poor observation efficiency.}} \citep{kataza_comics:_2000,sako_improvements_2003}. \citet{okamoto_improved_2003} reported that nodding was not necessary for bright objects since the residual pattern in a chop-subtracted image was almost negligible. Thus, nodding was not performed in the observation.} The exposure time for a single beam was about 0.964\,s with an overhead of about 0.120\,s. The overhead time corresponds to the time required for beam-switching. A single FITS data cube was composed of 22 exposures. The sequence was repeated 16 times. In total, 286 exposures were obtained.

In the ``slow-scanning'' observation, the chopping throw angle was set to $0\arcdeg$. The images were continuously obtained with the same exposure time and the same overhead time\footnote{This overhead time for beam-switching is practically not required in the ``slow-scanning'' observation. The observational efficiency can be improved by about 11\% by eliminating the overhead time.} as the chopping observation. The scanning speed was about {$0.{\!\arcsec}063\mathrm{s^{-1}}$} and the position angle of the scan was $-90\arcdeg$ (East to West). \textcolor{20170927}{Although the scanning PA was set orthogonal to the chopping PA by chance, we are confident that the result was not affected.} A single FITS data cube was composed of 88 exposures. The sequence was repeated 5 times. While 440 exposures were obtained in total, the first 286 exposures were used for the performance verification.

\begin{table*}[t]
  \centering
  \caption{Observation Log}
  \label{tab:obslog}
  \begin{tabular}[t]{lrr}
    \hline\hline
    & \multicolumn{1}{l}{Chopping Observation}
    & \multicolumn{1}{l}{``Slow-Scanning'' Observation} \\\hline
    Date
    & 2015-07-27 (UTC) & 2015-07-27 (UTC) \\
    Observing Time
    & 06:25--06:52 & 10:13--10:21 \\
    Airmass
    & 1.007--1.029 & 1.318--1.358 \\
    Filter
    & 11.7$\,{\rm \mu m}$ & 11.7$\,{\rm \mu m}$ \\
    Chopping Throw
    & $10\arcsec$ & $0\arcsec$ \\
    Chopping PA
    & $0\arcdeg$ & --- \\
    Chopping Cycle
    & $2.167\,$s & --- \\
    Scanning Speed
    & --- & \textcolor{20170927}{$0.{\!\arcsec}063\mathrm{s}^{-1}$} \\
    Scanning PA
    & --- & $-90\arcdeg$ \\
    Exposure Time
    & $0.964\,$s/frame & $0.964\,$s/frame \\
    Overhead Time
    & $0.120\,$s/frame & $0.120\,$s/frame \\
    Total Exposures
    & 286\,frames & 286\,frames \\
    \hline
  \end{tabular}
\end{table*}

\begin{figure*}[tp]
  \centering
  \plottwo{./figs/1719+186_chop_single.pdf}{./figs/1719+186_chop.pdf}
  \plottwo{./figs/1719+186_scan2_single.pdf}{./figs/1719+186_scan2.pdf}
  \caption{Images obtained in the chopping and ``slow-scanning'' observations. Panel (A) shows one of the combined chopping pair images, while Panel (B) shows the combined image. Panel (C) shows a single exposure image from the non-low-rank FITS cube (\textit{see}, text), while Panel (D) illustrates the combined image in the ``slow-scanning'' observation.}
  \label{fig:resultimages}
\end{figure*}

\begin{figure*}[tp]
  \centering
  \plottwo{./figs/1719+186_chop_hc.pdf}{./figs/1719+186_scan2_hc.pdf}
  \caption{Panels (B) and (D) of Figure~\ref{fig:resultimages} in different color scales.}
  \label{fig:resultimages_highcontrast}
\end{figure*}

\begin{figure*}[tp]
  \centering
  \plotone{./figs/photometry_statistics_intensity.pdf}
  \plotone{./figs/photometry_statistics_stddev.pdf}
  \caption{Statistics of the photometric measurements for the chopping and ``slow-scanning'' observations. Panel (A) shows the distributions of the measured intensities, while Panel (B) shows those of the standard deviations in the blank sky region. The red solid bars show the results for the chopping observation. The blue cross-hatched bars indicate the results for the ``slow-scanning'' observation.}
  \label{fig:singlestats}
\end{figure*}

\begin{table*}[tp]
  \centering
  \caption{Details of the Photometry on the Combined Images}
  \label{tab:photresults}
  \begin{tabular}{lrr}
    \hline\hline
    & \multicolumn{1}{c}{Chopping}
    & \multicolumn{1}{c}{``Slow-Scanning''} \\\hline
    Number of Stacked Frames
    & 286 & 286 \\
    Aperture Radius (pixel)
    & 8 & 8 \\
%    Residual Background
%    & $3.51\,$ADU & $24.52\,$ADU \\
    Standard Deviation of Blank Sky Region (ADU)
    & $11.05$ & $5.26$ \\
    Integrated Intensity ($10^3$ADU)
    & $59.34{\pm}0.16$ & $54.37{\pm}0.07$ \\
%    Photometric Error
%    & $156.88\,$ADU & $74.587\,$ADU \\
    Signal-to-Noise Ratio
    & 380 & 729 \\
    \hline
  \end{tabular}
\end{table*}


\subsection{Results of the Conventional Chopping Observation}
\label{sec:obs:chopping}
The data obtained in the chopping observation was composed of 286 exposures, or 143 chopping pairs. Subtracted images were derived from each chopping pair. \textcolor{20170927}{The COMICS system reads the detector current from all 16 channels at the same time. Uncertainty in the readout circuit causes the same noise pattern among the 16 channels (hereafter, the common-mode noise). The common-mode noise among output channels was subtracted using the channels where the object is not located \citep{sako_improvements_2003,okamoto_subaru_2012}.} Positive and negative sources in the subtracted images were combined by shift-and-add. No flat frame correction was applied, to keep the noise level from being affected by the uncertainty of a flat frame. Panel (A) of Figure~\ref{fig:resultimages} shows one of the combined chopping pair images. The intensity of the source was measured by aperture photometry using IRAF\footnote{IRAF is distributed by the National Optical Astronomy Observatories, which are operated by the Association of Universities for Research in Astronomy, Inc., under cooperative agreement with the National Science Foundation. distributed. IRAF is available in \url{http://iraf.noao.edu/}.}. \textcolor{20170927}{The blue solid lines indicate the photometric aperture size, while the annulus region indicated by the white dashed lines shows the blank sky region, where the background noise was evaluated.} The distributions of the intensities and the standard deviations of the background emission\footnote{The standard deviations were measured in the images after shift-and-add and multiplied by $\sqrt{2}$ to compare with those in the ``slow-scanning'' observation.} are illustrated in Figure~\ref{fig:singlestats}. The combined image was derived by averaging over 143 images, shown in Panel (B) of Figure~\ref{fig:resultimages}. Photometric results of the combined image are summarized in Table~\ref{tab:photresults}.


\subsection{Results of the ``Slow-Scanning'' Observation}
\label{sec:obs:slowscan}
First, we applied the same data reduction method on the data obtained in the ``slow-scanning'' observation and measured the standard deviation in a blank sky region. The standard deviation was almost the same as in the chopping observation. Thus we conclude that the conditions in the two observations were similar enough to be compared.

The non-redundant data cube was made from the 286 frames obtained during ``slow-scanning''. The adopted reduction parameters were as follows: $(r,\,\xi,\,k,\,R) = (12,\,3,\,3,\,4)$\footnote{In the \texttt{BIRSVD} calculation, a square of the second-order derivative with eight-order accuracy was selected as a regularization term. The strength of the regularization was set to 0.01. Refer to \citet{das_fast_2011} for details.}. The convergence of the algorithm was sufficiently fast, so that the iteration was terminated after 15 loops. No flat frame correction was applied. Panel (C) of Figure~\ref{fig:resultimages} shows one of the frames in the non-redundant data cube. The intensity of the source in each frame was measured by aperture photometry with the same parameters as in the chopping observation. \textcolor{20170927}{The blue solid circles and the white dashed lines are the same as in the chopping observation. The distributions of the measured intensities and the standard deviations in the blank sky regions are summarized in Figure~\ref{fig:singlestats}.} The 286 frames were combined by shift-and-add based on the motion of the telescope. The combined image is displayed in Panel (D) of Figure~\ref{fig:resultimages}. The intensity of the source in the combined image was measured by aperture photometry. The results are listed in Table~\ref{tab:photresults}.

% \begin{table}[tp]
%   \centering
%   \caption{mGoDec Reduction Parameters}
%   \label{tab:redparam}
%   \begin{tabular}{lr}
%     \hline\hline
%     Rank Limit ($r$)            & 12 \\
%     Detection Threshold ($\xi$) & 3 \\
%     Drop Limit ($k$)            & 3\,pixel \\
%     Dilation Radius ($R$)       & 4\,pixel \\
%     \hline
%   \end{tabular}
% \end{table}

% \begin{figure*}[tp]
%   \centering
%   \plotone{./figs/1719+186_movie.pdf}
%   \caption{Snapshots of the source movement. These images are extracted from the non-low-rank FITS data cube. The numbers indicate the chronological order of the images. Panel 1 shows the first exposure of the FITS data cube. The following images show the exposure with the interval of about 20\,s. This figure is available as an \textit{animation}.}
%   \label{fig:snapshots}
% \end{figure*}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../manuscript"
%%% End:
