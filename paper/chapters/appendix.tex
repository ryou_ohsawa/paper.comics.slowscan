\section{Short-Timescale Stability of the Flat Frame}
\label{sec:flatframe}
\textcolor{20170927}{In the proposed algorithm, we assume that the flat frame is stable in a short-time scale. The stability of the flat frame was evaluated based on the data we obtained. We used 440 exposures obtained with \textit{Subaru}/COMICS. First, we derived the time-sequence of the intensity averaged over the field-of-view.}
\begin{equation}
  \label{eq:timeseq}
  \textcolor{20170927}{\bar{I}_m = \frac{1}{N_XN_Y}\sum_{i,j}I_{i,j,m}.}
\end{equation}
\textcolor{20170927}{In Figure \ref{fig:flatframe}, the intensities of individual pixels are plotted against $\bar{I}_m$. The distributions of the data points are well-approximated by a linear function, the slope of which corresponds to the pixel response. We derived the slope for each pixel by fitting. The fitted curves are shown by the black solid lines in Figure~\ref{fig:flatframe}. The standard deviation of the slopes, which indicates a typical pixel-to-pixel fluctuation in the flat frame, was about $4.1\%$.}

\begin{figure}
  \centering
  \plotone{./figs/append_flatframe.pdf}
  \caption{Pixel counts plotted against the count averaged over the field-of-view. Statistical errors are as small as the symbol sizes. The black solid lines indicate the fitted curves (\textit{see}, text).}
  \label{fig:flatframe}
\end{figure}

\textcolor{20170927}{Figure~\ref{fig:flatframe} shows deviations from the fitting. The deviations were about 4 times larger than those expected from the photon Poisson noise. Part of the deviations should be attributed to the short-timescale variability in the flat frame. We assume that all the deviations originate from the short-timescale variability in the flat frame. The fractional deviation from the fit is derived by}
\begin{equation}
  \label{eq:deviation}
  \textcolor{20170927}{
    \delta_{i,j,m} =
    \frac{I_{i,j,m} - I^\mathrm{fit}_{i,j,m}}{I^\mathrm{fit}_{i,j,m}},}
\end{equation}
\textcolor{20170927}{where $I^\mathrm{fit}_{i,j,m}$ is the value derived by the fitting at $(i,\,j,\,t_m)$. The standard deviation of $\delta_{i,j,m}$, which indicates an upper limit on the short-timescale variation in the flat frame, was about $0.037\%$. Thus, we conclude that the flat frame was approximately constant in time during our observations.}


\section{Efficiency in Chopping Observation}
\label{sec:chopping}
In the conventional chopping observation, we obtain a pair of images in different positions A and B, with a time separation of ${\delta}t$. By differentiating the pair, the signal from astronomical objects is extracted.
\begin{equation}
  \label{eq:chopping}
  I_{i,j}^{\rm A}(t) {-} I_{i,j}^{\rm B}(t{+}{\delta}t)
  = f_{i,j} \bigl(
      {\mathcal S}^{\rm A}_{i,j}{-}{\mathcal S}^{\rm B}_{i,j}
  {+}{\mathcal A}_{i,j}(t){-}{\mathcal A}_{i,j}(t{+}{\delta}t)
  \bigr),
  \label{eq:chopping_approx}
\end{equation}
where the differences in emission from the instrument and the telescope between the chopping positions is neglected for the sake of simplicity. In a limit of ${\delta}t \rightarrow 0$, atmospheric emission is perfectly canceled out. Practically, ${\delta}t$ should be a short but finite value. The time separation required to eliminate the variation in the atmospheric emission depends on the power spectrum of ${\mathcal A}(t)$. Suppose that the Fourier transformation of ${\mathcal A}(t)$ is $\tilde{\mathcal A}(f)$, the noise power spectrum (NPS) of ${\mathcal A}(t)$ is described by
\begin{equation}
  \label{eq:esd_A}
  {\rm NPS}_{\mathcal A}(f)
  = \tilde{\mathcal A}(f)\tilde{\mathcal A}^\dagger(f),
\end{equation}
where $X^\dagger$ is the complex-conjugate of $X$. In practice, the observed atmospheric signals are integrated over an exposure time and averaged over exposures. Taking into account the integration time of $\Delta{t}\,({<}\delta{t})$ and the number of exposures $N$, an effective atmospheric emission component is given by the convolution of ${\mathcal A}(t)$ and a window function:
\begin{equation}
  \label{eq:eff_A}
  {\mathcal A^*}(t) = \int_{-\infty}^{\infty}
  {\mathcal A}(t')\frac{1}{N}\sum_{n=0}^{N-1}
  {\rm rect}\left(\frac{t-2n{\delta}t-t'}{\Delta{t}}\right)
  {\rm d}t',
\end{equation}
where ${\rm rect}(x)$ is a rectangular function. Equation~(\ref{eq:esd_A}) describes the noise energy in the frequency range between $f$ and $f+{\rm d}f$. The differentiation of ${\mathcal A^*}(t)$ with the time separation of ${\delta}t$ is defined by ${\alpha}(t,{\delta}t)$:
\begin{equation}
  \label{eq:alpha}
  {\alpha}(t,{\delta}t) = {\mathcal A^*}(t) - {\mathcal A^*}(t+{\delta}t).
\end{equation}
The energy spectral distribution of ${\alpha}(t,{\delta}t)$ is given as:
\begin{eqnarray}
  {\rm NPS}_{\alpha}(f \,|\, {\delta}t,{\Delta}t, N)
  & \propto
  & {\rm NPS}_{\mathcal A}(f)
    \,\,2\bigl(1 - \cos 2\pi f\,{\delta}t\bigr)
    \Bigl|{\rm sinc} \left(f\Delta{t}\right) \Bigr|^2
    \left|
    \frac{1}{N}\sum_{n=0}^{N-1}{\rm e}^{4{\pi}in{\delta}tf}
    \right|^2, \nonumber\\
  & =
  & {\rm NPS}_{\mathcal A}(f)
    \cdot\mathcal{F}_1(f\delta{t})
    \cdot\mathcal{F}_2(f\Delta{t})
    \cdot\mathcal{F}_3(N,f\delta{t}),
    \label{eq:esd_alpha}\\
  && \text{where}~
     \left\{~~\begin{array}{l}
      \mathcal{F}_1(f\delta{t})
      = 2\bigl(1 - \cos 2\pi f\,{\delta}t\bigr),\\
      \mathcal{F}_2(f\Delta{t})
      = \Bigl|{\rm sinc} \left(f\Delta{t}\right) \Bigr|^2,\\
      \mathcal{F}_3(N,f\delta{t})
      = \left|\frac{1}{N}\sum_{n=0}^{N-1}{\rm e}^{4{\pi}in{\delta}tf}\right|^2
    \end{array}\right.
\end{eqnarray}
Equation~(\ref{eq:esd_alpha}) indicates that the chopping procedure is recognized as a product of three filtering functions. The filter $\mathcal{F}_1$, a differentiation filter, works as a high-pass filter. As long as $2\pi f\,{\delta}t \simeq 0$, noise components are suppressed. Instead high-frequency components are increased by this filter. The filter $\mathcal{F}_2$, an integration filter, works as a low-pass filter. Noise components at higher than about $\Delta{t}^{-1}\,$Hz are suppressed. The filter $\mathcal{F}_3$ is an averaging filter, which ensures that the integral of ${\rm NPS}_{\alpha}$ over $f$ is almost proportional to $N^{-1}$. The averaging filter does nothing when $N=1$, but it suppress the noise components between $(2N{\delta}t)^{-1}$ and $(N-1)(2N{\delta}t)^{-1}$\,Hz.

Figure~\ref{fig:nps} illustrates how the chopping works. We assume that ${\rm NPS}_{\mathcal A}(f) \propto f^{-1}$. High-frequency noise components are suppressed by integration. The blue dashed line indicates the noise spectrum after 1.0\,s integration. High-frequency noise components are suppressed. The solid line indicates the NPS after the differentiation of 0.5\,Hz chopping pairs ($\delta{t} = 1.0\,\mathrm{s}$). The noise components below about 0.2\,Hz are suppressed by chopping, while noise components around 0.5\,Hz are increased by a factor of around 2. The red solid line indicates the NPS after stacking 5 chopping pairs. Low-frequency noise components are well-suppressed by stacking.

\begin{figure*}
  \centering
  \plotone{./figs/append_chopping.pdf}
  \caption{Normalized noise power spectra in the case where ${\rm NPS}_{\mathcal A}(f) \propto f^{-1}$ (\textit{see}, text). The chopping-time separation $\delta{t}$ is set $1.0\,$s (0.5\,Hz chopping). The dashed lines show the power spectra for a single exposure before the subtraction of chopping pairs. The blue solid lines indicate the power spectra after the differentiation of chopping pairs. The red solid line shows the noise power spectrum after combining 5 chopping pairs.}
  \label{fig:nps}
\end{figure*}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../manuscript"
%%% End:
