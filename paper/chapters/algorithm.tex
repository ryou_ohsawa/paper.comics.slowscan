\subsection{``Slow-Scanning'' Observation}
\label{sec:slow-scanning}
The ``slow-scanning'' method is designed to extract scientific signals from a sequence of images. Figure~\ref{fig:scheme} shows a schematic view of the ``slow-scanning'' observation. In the observation, images are continuously obtained as movie data, while a telescope is slowly moved. The integration time of a single exposure should be shorter than the typical timescale of variation of atmospheric emission (${\lesssim}1\,$s). The scanning speed of the telescope should be slow enough, so that the image of the scientific target should not be affected by the telescope movement. For a point-source object, the scanning speed $v$ should be slower than ${\sim}{\theta}t^{-1}$, where $\theta$ is the pixel scale of a detector and $t$ is the integration time per exposure. The observation should be continued until the location of the target is entirely changed. The total observing time $T$ should be a few times larger than $\Theta {v^{-1}}$, where $\Theta$ is the angular size of the target. The movie data are compiled into a three-dimensional FITS data cube, which is the typical data structure in ground-based mid-infrared observations.

\begin{figure}[tp]
  \centering
  \plotone{./figs/schematic_scanning.pdf}
  \caption{Schematic view of the ``Slow-Scanning'' observation. The gray rectangles indicate the field of view of a single exposure. Images are continuously obtained by slightly shifting the field of view.}
  \label{fig:scheme}
\end{figure}


\subsection{Data Reduction Algorithm}
\label{sec:data-reduction}
This section provides an algorithm to extract scientific information from the obtained data cube. First, characteristics of data obtained in the ground-based mid-infrared observation are described. An image with $N_X{\times}N_Y$ pixels is produced in every exposure. The position of a pixel is designated by an index $i$ and $j$. The images are continuously obtained: The total number of exposures is $M$. The time of the $m$th exposure is denoted by $t_m$. The signal at $(i,\,j)$ and the time $t_m$ is denoted by $I_{i,j,m} := I_{i,j}(t_m)$.

We assume that the signal in each pixel consists of emission from an astronomical object, atmospheric emission, emission from the telescope and instrument, and instrumental bias. The emission from the object (${\mathcal S}$) is assumed to be constant in time, but it changes its location with movement of the telescope. The atmospheric emission (${\mathcal A}$) is assumed to rapidly vary with time but marginally depends on $i$ and $j$. The emission from the telescope and the instrument (${\mathcal T}$) is expected to be constant in time while the temperature of the system remains the same. We assume that the bias, such as dark current, (${\mathcal D}$) is constant in time, but can vary with position in the image.

The signal originating from photons is subject to a pixel-to-pixel variation in the response of the detector, or the flat frame. \textcolor{20170927}{In general, the flat frame ($f$) is time-variant since the detector and a readout system are not stable. \citet{nakamura_method_2016}, however, successfully subtracted background emission with the ``weighted average'' method, where the stability of the flat frame was assumed. Thus, we expect that the flat frame is approximately stable over a time scale of a few minutes. For the sake of simplicity, we assume that $f$ is constant in time during the observation. The validity of this assumption is discussed in Section~\ref{sec:applicability} and Appendix~\ref{sec:flatframe}.} Thus, the signal value at $(i,\,j)$ in the $m$th exposure is described as
\begin{equation}
  \label{eq:signal}
  I_{i,j,m}
  = f_{i,j}\bigl(
    {\mathcal A}_{i,j,m}
    + {\mathcal T}_{i,j}
    + {\mathcal S}_{i,j,m}
  \bigr) + {\mathcal D}_{i,j}.
\end{equation}
Statistical noise components (e.g., readout noise) are dropped here for the sake of simplicity. In a typical mid-infrared observation, the intensity of ${\mathcal S}$ is much lower than those of ${\mathcal A}$ and ${\mathcal T}$. A precise subtraction of ${\mathcal A}$ and ${\mathcal T}$ is required to extract the signal from astronomical objects. In $N$-band observations (${\sim}10\,\mu$m), ${\mathcal A}$ is as strong as ${\mathcal T}$, while ${\mathcal A}$ becomes much stronger at longer wavelengths.

Here, we describe how to extract the scientific signal of an astronomical object from the obtained data. The emission from the telescope and the instrument $f_{i,j}{\mathcal T}_{i,j}$ and the bias component ${\mathcal D}_{i,j}$ are assumed to be almost constant in time. The atmospheric component $f_{i,j}{\mathcal A}_{i,j,m}$ rapidly varies in time, but it is expected to be smooth in the spatial dimensions. These components are expected to have high redundancy in the obtained data. Instead, the scientific component $f_{i,j}{\mathcal S}_{i,j,m}$ contains the spatially-localized signal, which moves with time. This component is expected to have much less redundancy than the other components. Thus, an algorithm, which isolates non-redundant components from redundant components, will extract $f_{i,j}{\mathcal S}_{i,j,m}$ from the movie data obtained in the ``slow-scanning'' observation.

A low-rank and sparse matrix decomposition is widely used to extract non-redundant components. Several algorithms for \textcolor{20170927}{the matrix decomposition} have been developed \citep[e.g.,][]{candes_robust_2009,keshavan_gradient_2009,keshavan_matrix_2009}.

We focus on the Go Decomposition \citep[\texttt{GoDec};][]{zhou_godec:_2011}, which is a fast and robust implementation of the low-rank and sparse matrix decomposition. \texttt{GoDec} approximates the matrix $A$ by $L{+}S$, where $L$ is a low-rank matrix or a redundant component, and $S$ is a sparse matrix or a non-redundant component. The rank of the low-rank matrix $L$ is defined by $r$. The sparse matrix $S$ contains $k$ non-zero components. The matrices $L$ and $S$ are alternatively updated: $L$ is given by the low-rank matrix approximation of $A{-}S$, while $S$ consists of the non-zero subset of the first $k$ largest entries of $|A{-}L|$. \citet{morii_data_2017} applied the \texttt{GoDec} method to astronomical movie data and demonstrated that \texttt{GoDec} successfully extracts fast transient signals from background emission.

We propose a reduction procedure for the ``slow-scanning'' observation, which basically follows the procedure of \texttt{GoDec} but is slightly modified to work on masked data (\textit{hereafter}, referred as to masked GoDec or \texttt{mGoDec}). The procedure is described in Table~\ref{algo:maskedgodec}. Figure~\ref{fig:ssdec} illustrates a schematic view of the main iteration part in the \texttt{mGoDec} procedure. While the bilateral random projections method is implemented for the low-rank matrix approximation in \texttt{GoDec}, \texttt{mGoDec} uses Bi-Iterative Regularized Singular Value Decomposition \citep[\texttt{BIRSVD};][]{das_fast_2011,das_birsvd_2011}\footnote{The \texttt{BIRSVD} software is available at \url{http://www.mat.univie.ac.at/~neum/software/birsvd/}}.

\textcolor{20170927}{First, the obtained data $I_{i,j,m}$, a three-dimensional data cube $\left(\mathbb{R}^{N_X} {\times} \mathbb{R}^{N_Y} {\times} \mathbb{R}^M\right)$, is rearranged into a two-dimensional matrix $A_{n,m}\,\left(\mathbb{R}^{N_XN_Y} {\times} \mathbb{R}^M\right)$  (Line~1--3 in Table~\ref{algo:maskedgodec}).} The \texttt{BIRSVD} method generates a low-rank approximation of the matrix weighted by a weighting matrix $W$ (Line~5). The weighting matrix $W$ is a binary mask matrix such that $W_{n,m} = 1$ when the $(n,\,m)$-element of a matrix is masked. The weighting matrix $W$ is updated by ``thresholding'' (Lines~6--12), where $W_{n,m} = 1$ if $|A{-}L|_{n,m} > \xi\sigma$, where $\sigma$ is the standard deviation of $A{-}L$ \textcolor{20170927}{and $\xi$ is a parameter to tune the threshold}. In Lines~13--15, the weighting matrix is converted to the 3-dimensional image cube $Z$, which has the same shape as the original data cube. The $m$th image in $Z$ is indicated by $Z_{*,*,m}$. Each image of $Z$ is processed as follows; Small masks, which are a size\footnote{The size of a mask is defined by the number of connected masked pixels.} smaller than $k$, are rejected in Line~17, and the remaining masks are dilated by a disk of radius $R$ (Line~18). Then, the image cube $Z$ is converted again into $W$ (Lines~20--22). This procedure is iterated until the remaining component $\sum_{n,m}W_{n,m}\left|A_{n,m}{-}L_{n,m}\right|^2$ becomes smaller than the given threshold $\epsilon$. \textcolor{20170927}{Finally, the background subtracted data cube $\hat{I}_{i,j,m}$ is derived from the subtraction $A-L$ (Lines~24--26). The data cube $\hat{I}_{i,j,m}$ is the movie data where the object is moving across the field-of-view. To obtain a combined image, a shift-and-add operation should be applied on $\hat{I}_{i,j,m}$. Note that the data cube $\hat{I}_{i,j,m}$ mainly consists of $f_{i,j}{\mathcal S}_{i,j,m}$. Thus, each frame of $\hat{I}_{i,j,m}$ should be divided by a flat frame, if available.}


\begin{table*}[p]
  \caption{Slow-Scanning Decomposition}\label{algo:maskedgodec}
  \centering
  \begin{tabular}{rp{.8\linewidth}}
    \hline\hline
    \multicolumn{2}{l}{\textbf{Input:} $I_{i,j,m},r,\xi,\epsilon,k,R$} \\
    \multicolumn{2}{l}{\textbf{Output:} $\hat{I}_{i,j,m}$} \\
    \multicolumn{2}{l}{\textbf{Local Variables:}
    $A,L,W \in \mathbb{R}^{N_XN_Y}{\times}\mathbb{R}^M$,
    $Z \in \mathbb{R}^{N_X}{\times}\mathbb{R}^{N_Y}{\times}\mathbb{R}^M$} \\
    \multicolumn{2}{l}{\textbf{Initialize:}
    $A \leftarrow \mathbf{0}$, $L \leftarrow \mathbf{0}$,
    $W \leftarrow \mathbf{0}$, $Z \leftarrow \mathbf{0}$} \\
    1 &\textbf{for}
        $i \leftarrow 1$ \textbf{to} $N_X$
        and $j \leftarrow 1$ \textbf{to} $N_Y$
        and $m \leftarrow 1$ \textbf{to} $M$\textbf{:} \\
    2 &\quad $A_{i+N_Y(j-1),m} \leftarrow I_{i,j,m}$ \\
    3 &\textbf{end for} \\
    4 &\textbf{While}
        {$\sum_{n,m}W_{n,m}|A{-}L|_{n,m}^2 > \epsilon$} \textbf{do} \\
    5 &\quad $L \leftarrow {\rm BIRSVD}(A,W,r)^{\dagger 1}$ \\
    6 &\quad \textbf{for}
        $n \leftarrow 1$ \textbf{to} $N_X{\times}N_Y$
        and $m \leftarrow 1$ \textbf{to} $M$\textbf{:} \\
    7 &\qquad \textbf{if}
        $\left|A-L\right|_{n,m} > \xi\sigma$ \textbf{then:}\\
    8 &\quad\qquad $W_{n,m} \leftarrow 1$ \\
    9 &\qquad \textbf{else:} \\
    10&\quad\qquad $W_{n,m} \leftarrow 0$ \\
    11&\qquad \textbf{end if} \\
    12&\quad \textbf{end for} \\
    13&\quad \textbf{for}
        $i \leftarrow 1$ \textbf{to} $N_X$
        and $j \leftarrow 1$ \textbf{to} $N_Y$
        and $m \leftarrow 1$ \textbf{to} $M$\textbf{:} \\
    14&\qquad $Z_{i,j,m} \leftarrow W_{i+N_Y(j-1),m}$ \\
    15&\quad \textbf{end for} \\
    16&\quad \textbf{for} $m\leftarrow1$ \textbf{to} $M$\textbf{:}\\
    17&\qquad
        $Z_{*,*,m} \leftarrow {\rm DropSmallMask}(Z_{*,*,m},k)^{\dagger 2}$ \\
    18&\qquad
        $Z_{*,*,m} \leftarrow {\rm BinaryDilation}(Z_{*,*,m},R)^{\dagger 3}$ \\
    19&\quad \textbf{end for} \\
    20&\quad \textbf{for}
        $i \leftarrow 1$ \textbf{to} $N_X$
        and $j \leftarrow 1$ \textbf{to} $N_Y$
        and $m \leftarrow 1$ \textbf{to} $M$\textbf{:} \\
    21&\qquad $W_{i+N_Y(j-1),m} \leftarrow Z_{i,j,m}$ \\
    22&\quad \textbf{end for} \\
    23&\textbf{end while} \\
    24&\textbf{for}
        $i \leftarrow 1$ \textbf{to} $N_X$
        and $j \leftarrow 1$ \textbf{to} $N_Y$
        and $m \leftarrow 1$ \textbf{to} $M$\textbf{:} \\
    25&\quad $\hat{I}_{i,j,m} \leftarrow (A-L)_{i+N_Y(j-1),m}$ \\
    26&\textbf{end for} \\
    \hline\hline
    \multicolumn{2}{l}{$^{\dagger 1}$obtain an $r$-lank approximation of the matrix $A$ with the weighting matrix $W$.} \\
    \multicolumn{2}{l}{$^{\dagger 2}$erase segments whose sizes are smaller than $k$ pixels in $Z_{*,*,m}$.} \\
    \multicolumn{2}{l}{$^{\dagger 3}$apply a binary-dilation process on $Z_{*,*,m}$ with the radius of $R$.}
  \end{tabular}
\end{table*}

\begin{figure*}[tp]
  \centering
  \plotone{./figs/schematic_ssdec.pdf}
  \caption{Schematic view of the Slow-Scanning Decomposition Procedure.}
  \label{fig:ssdec}
\end{figure*}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../manuscript"
%%% End:
